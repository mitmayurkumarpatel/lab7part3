package Vegetables;

/**
 *
 * @author mitpatel
 */
public abstract class  Vegetables
{

    private String colour;
    private double size;

    //constructor
    public Vegetables(String colour ,double size ) {
        this.colour=colour;
        this.size=size;
    }

    //getter
    public String getColour(){
        return this.colour;
    }

    public double getSize(){
        return this.size;
    }

    //abstract method
    public abstract void isRipe();

   
}
