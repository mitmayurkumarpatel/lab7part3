/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetables;

import java.util.ArrayList;

/**
 *
 * @author mitpatel
 */
public class VegetableSimulation 
{
    public static void main(String[] args) 
    {
        
        ArrayList<Carrot> carrot = new ArrayList<Carrot>();
        ArrayList<Beet> beet= new ArrayList<Beet>();

        carrot.add(new Carrot("red", 5.0));
        carrot.add(new Carrot("Orange", 3.6));

        beet.add(new Beet("red", 8.0));
        beet.add(new Beet("Orange", 3.2));

        carrot.get(0).isRipe();
        carrot.get(1).isRipe();

        beet.get(0).isRipe();
        beet.get(1).isRipe();
        
        


    }
    
}
